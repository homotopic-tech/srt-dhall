{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Media.Subtitles.SRT.Dhall
  ( srtDecoder,
  )
where

import qualified Data.Attoparsec.Text as A
import Data.Either.Validation
import Data.Functor.Contravariant
import Data.Text as T
import Dhall
import Formatting
import Media.Subtitles.SRT
import Media.Subtitles.SRT.Attoparsec
import Media.Subtitles.SRT.Formatting

instance FromDhall SRT where
  autoWith options = srtDecoder options

srtDecoder :: InputNormalizer -> Decoder SRT
srtDecoder opts =
  Decoder {..}
  where
    textDecoder :: Decoder Text
    textDecoder = autoWith opts

    extract expression =
      case Dhall.extract textDecoder expression of
        Success x -> case A.parseOnly parseSRT x of
          Left exception -> Dhall.extractError (T.pack $ show exception)
          Right k -> Success k
        Failure e -> Failure e

    expected = Dhall.expected textDecoder

instance ToDhall SRT where
  injectWith = fmap (contramap (sformat srtf)) injectWith
