# Changelog for srt-dhall

## v0.1.0.0

* Add `FromDhall` and `ToDhall` instances for `SRT`.
